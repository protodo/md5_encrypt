package com.example.md5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView lbltxt;
    Button btnprueba;
    EditText txtMD5;
    String Smd5, getSmd5;
    MD5 md5 = new MD5();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lbltxt = findViewById(R.id.lbltxt);
        btnprueba = findViewById(R.id.btnprueba);
        txtMD5 = findViewById(R.id.txtMD5);

        btnprueba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Smd5 = txtMD5.getText().toString();

               getSmd5 = md5.encrypt(Smd5);

               lbltxt.setText(getSmd5);
            }
        });
    }
}